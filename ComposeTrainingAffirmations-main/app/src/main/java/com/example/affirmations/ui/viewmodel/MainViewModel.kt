package com.example.affirmations.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class MainViewModel : ViewModel(){

    // MutableStateFlow -> Llista mutable
    // StateFlow -> Llista no mutable
    // asStateFlow -> Comprovar si cambia

    private val _datasource = Datasource().loadAffirmations()

    private var _affirmationsList = MutableStateFlow<List<Affirmation>>(mutableListOf())
    val affirmationList: StateFlow<List<Affirmation>> = _affirmationsList.asStateFlow()

    fun loadList()
    {
        _affirmationsList.value = _datasource
    }

}