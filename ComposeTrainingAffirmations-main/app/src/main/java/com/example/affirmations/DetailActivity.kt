package com.example.affirmations

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Debug
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Mail
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme
import com.example.affirmations.ui.viewmodel.DetailViewModel

class DetailActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val context = LocalContext.current
            val intent = (context as DetailActivity).intent

            val detailViewModel = DetailViewModel()
            detailViewModel.loadAffirmation(intent.getIntExtra("id", 1))

            val affirmation by detailViewModel.affirmation.collectAsState()

            AffirmationsTheme() {
                Surface(color = MaterialTheme.colors.background,
                            modifier = Modifier.fillMaxSize()) {

                    Detail(affirmation)
                }
            }
        }
    }
}

private fun Context.sendMail(to: String, subject: String) {
    try {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(to))
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        // Handle case where no email app is available
        Log.w("Error","Error no email app")
    } catch (t: Throwable) {
        // Handle potential other type of exception
        Log.w("Error","Error email")
    }
}


private fun Context.dial(phone: String) {
    try {
        val intent = Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null))
        //val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$phone"))
        startActivity(intent)
    } catch (t: Throwable) {
        // Handle potential exceptions
        Log.w("Error","Error phone")
    }
}


@Preview
@Composable
private fun DetailPreview() {
    Detail(
        affirmation = Affirmation(
            R.string.affirmation1,
            R.drawable.image1,
            R.string.extension1,
            1
        )
    )
}

@Composable
private fun Detail(
    affirmation: Affirmation,
    modifier: Modifier = Modifier
) {
    Column(modifier = modifier) {
        Image(
            painter = painterResource(id = affirmation.imageResourceId),
            contentDescription = stringResource(id = affirmation.stringResourceId),
            modifier = modifier
                .padding(10.dp)
                .fillMaxWidth()
                .height(200.dp)
            //.weight(0.5f),
            //contentScale = ContentScale.Fit
        )
        Text(
            text = stringResource(id = affirmation.stringResourceId),
            modifier = modifier
                .padding(30.dp, 0.dp, 30.dp, 10.dp)
            //.weight(0.02f)

        )
        Text(
            text = stringResource(id = affirmation.stringExtendible),
            modifier = modifier
                .padding(20.dp, 0.dp, 20.dp, 10.dp)
            //.weight(0.5f)
        )

        Row(
            modifier = modifier
                //.weight(1f)
                .align(Alignment.End)
                .padding(0.dp, 0.dp, 20.dp, 0.dp)
        ) {
            IconButton(
                onClick = { DetailActivity().dial(phone = "12345678") }
                
            ) {
                Icon(
                    imageVector = Icons.Filled.Phone,
                    contentDescription = "dial"
                )

            }

            IconButton(onClick = { DetailActivity().sendMail(to = "gmail@gmail.com", subject = "Hello World!") }) {
                Icon(
                    imageVector = Icons.Filled.Mail,
                    contentDescription = "mail"
                )
            }
        }
    }
}