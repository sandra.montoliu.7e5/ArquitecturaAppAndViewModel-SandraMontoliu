package com.example.affirmations.ui.viewmodel

import androidx.lifecycle.ViewModel
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow

class DetailViewModel : ViewModel() {

    private val _datasource = Datasource().loadAffirmations()
    private lateinit var _affirmationsList : List<Affirmation>

    private var _affirmation = MutableStateFlow<Affirmation>(Affirmation(0,0,0,0))
    val affirmation: StateFlow<Affirmation> = _affirmation.asStateFlow()

    fun loadAffirmation(id: Int) {
        _affirmationsList = _datasource

        for (aff in _affirmationsList){
            if (aff.id == id)
                _affirmation.value = aff
        }

    }

}