/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.StringRes
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.affirmations.data.Datasource
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.AffirmationsTheme

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.sp
import com.example.affirmations.ui.viewmodel.MainViewModel


// affirmation by [...] collectAsState()

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            // TODO 5. Show screen
            AffirmationApp()
        }
    }
}

@Composable
fun AffirmationApp() {
    // TODO 4. Apply Theme and affirmation list
    val mainViewModel = MainViewModel()
    mainViewModel.loadList()
    val affirmation by mainViewModel.affirmationList.collectAsState()

    AffirmationsTheme() {
        //AffirmationList(affirmationList = Datasource().loadAffirmations())
        AffirmationList(affirmationList = affirmation)
    }
}

@Composable
fun AffirmationList(affirmationList: List<Affirmation>, modifier: Modifier = Modifier) {
    // TODO 3. Wrap affirmation card in a lazy column
    LazyColumn(modifier = Modifier.background(MaterialTheme.colors.background)) {
        items(affirmationList) { affirmation ->
            AffirmationCard(affirmation = affirmation)
        }
    }

}

@Composable
fun AffirmationCard(affirmation: Affirmation, modifier: Modifier = Modifier) {
    var expanded by remember { mutableStateOf(false) }
    val mContext = LocalContext.current
    val intent = Intent(mContext, DetailActivity::class.java)

    // TODO 1. Your card UI
    Card(
        modifier = modifier
            //.height(100.dp)
            //.fillMaxWidth()
            .padding(0.dp, 5.dp),
        elevation = 10.dp,
        shape = RoundedCornerShape(20.dp)
    ) {
        Column() {
            Row {
                Image(
                    painter = painterResource(id = affirmation.imageResourceId),
                    contentDescription = stringResource(id = affirmation.stringResourceId),
                    modifier = modifier
                        .fillMaxSize()
                        //.fillMaxWidth(1f)
                        .padding(0.dp, 0.dp, 0.dp, 0.dp)
                        .weight(1f),
                    contentScale = ContentScale.Crop
                )
                Text(
                    text = stringResource(id = affirmation.stringResourceId),
                    modifier = modifier
                        .padding(10.dp, 0.dp, 5.dp, 0.dp)
                        .weight(2f)
                        .align(alignment = Alignment.CenterVertically),
                    fontSize = 15.sp
                )
                AffirmationItemButton(
                    expanded = expanded,
                    onClick = { expanded = !expanded },
                    modifier = modifier
                        .weight(0.5f)
                )
            }
            if (expanded) {
                AffirmationExtendible(extendible = affirmation.stringExtendible)

                TextButton(
                    onClick = {
                        intent.putExtra("id", affirmation.id)
                        mContext.startActivity(intent)
                    },
                    colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color.Transparent),
                    border = BorderStroke(0.dp, Color.Transparent),
                    modifier = modifier
                        .align(Alignment.CenterHorizontally)
                ) {
                    Text(text = stringResource(id = R.string.seeMore),
                        color = Color.Blue)
                }
            }
        }
    }
}

@Preview
@Composable
private fun AffirmationCardPreview() {
    // TODO 2. Preview your card
    AffirmationCard(affirmation = Affirmation(R.string.affirmation1, R.drawable.image1,R.string.extension1,1))
}

@Composable
private fun AffirmationItemButton(
    expanded: Boolean,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    IconButton(onClick = onClick) {
        Icon(
            imageVector = Icons.Filled.ExpandMore,
            tint = MaterialTheme.colors.secondary,
            contentDescription = stringResource(id = R.string.expand_button_content_description)
        )
    }
}

@Composable
fun AffirmationExtendible(@StringRes extendible: Int, modifier: Modifier = Modifier) {
    Column(
        modifier = modifier.padding(16.dp, 10.dp, 16.dp, 0.dp)
    ) {
        Text(
            text = stringResource(id = R.string.expand_button_content_description),
            fontSize = 15.sp,
            modifier = modifier.padding(10.dp,0.dp, 0.dp, 0.dp)
        )
        Text(
            text = stringResource(id = extendible),
            fontSize = 15.sp,
            modifier = modifier.padding(20.dp,0.dp, 0.dp, 0.dp),
            maxLines = 2,
            overflow = TextOverflow.Ellipsis
        )
    }
}
